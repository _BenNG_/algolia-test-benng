const fs = require("fs");

fs.readFile("./hn_logs.tsv", "utf-8", (err, line) => {
  const date = line.slice(0, 19);
  const millisec = Date.parse(date);
  const value = line.slice(20);
  if (!millisec) {
    throw new Error("something went wrong with: ", date);
  }
});
