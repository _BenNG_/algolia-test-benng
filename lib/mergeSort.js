const merge = (leftKeys, rightKeys, hash) => {
  const results = [];
  let lIndex = 0;
  let rIndex = 0;
  const lLength = leftKeys.length;
  const rLength = rightKeys.length;

  // continue until we reach one array's end
  while (lIndex < lLength && rIndex < rLength) {
    const lKey = leftKeys[lIndex];
    const rKey = rightKeys[rIndex];
    // in the comparaison we have to separate the increment n order for the sort to be correct
    if (hash[lKey] < hash[rKey]) {
      results.push(lKey);
      lIndex++;
    } else {
      results.push(rKey);
      rIndex++;
    }
  }

  // if there are still some data inside do not forget those
  while (lIndex < lLength) {
    results.push(leftKeys[lIndex]);
    lIndex++;
  }
  while (rIndex < rLength) {
    results.push(rightKeys[rIndex]);
    rIndex++;
  }

  return results;
};

const sort = (keys, hash) => {
  // an array of one element is sorted
  if (keys.length <= 1) {
    return keys;
  }

  // we will use the mergeSort algorithm
  // first let's find the middle of the array
  const middle = Math.floor(keys.length / 2);

  // split the array in two parts
  // then apply the sort again
  const leftKeys = keys.slice(0, middle);
  const rightKeys = keys.slice(middle);

  const sortedLeftKeys = sort(leftKeys, hash);
  const sortedRightKeys = sort(rightKeys, hash);

  // when we reach the point where there is only one element in the array we merge
  return merge(sortedLeftKeys, sortedRightKeys, hash);
};
module.exports = {
  sort
};
