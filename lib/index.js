const fs = require("fs");
const readline = require("readline");
const { sort } = require("./mergeSort");

const compute = (query, options) => {
  const { isCountOnly = false, size = 0 } = options;
  return new Promise((resolve, reject) => {
    let accu = {};

    const rl = readline.createInterface({
      input: fs.createReadStream("./data/hn_logs.tsv"),
      crlfDelay: Infinity
    });

    rl.on("line", line => {
      const queryLength = query.length;
      const [l, rightPart] = line.split("\t");
      const date = l.slice(0, queryLength);

      // we filter the line we are interested in then we create an object with a query made on HN and the value is the time this query was made
      if (date === query) {
        const oldValue = accu[rightPart];
        if (oldValue) {
          accu[rightPart] = oldValue + 1;
        } else {
          accu[rightPart] = 1;
        }
      }
    });

    rl.on("error", error => {
      reject(error);
    });

    rl.on("close", () => {
      // here we have read all the lines of the file and the structure is ready to be sorted
      const keys = Object.keys(accu);
      const distinct = keys.length;
      // we only need the key to be sorted not the whole structure
      const sortedKeys = sort(keys, accu);

      // once sorted prepare the response. as the sort is asc, the more requested queries are at the end
      let res;
      if (isCountOnly) {
        res = { count: distinct };
      } else {
        res = [];
        for (
          let i = distinct - 1, iLength = distinct - (size + 1);
          i > iLength;
          i--
        ) {
          res.push({
            query: sortedKeys[i],
            count: accu[sortedKeys[i]]
          });
        }
      }
      resolve(res);
    });
  });
};

module.exports = {
  compute
};
