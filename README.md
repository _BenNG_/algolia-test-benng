# Algolia Technical test by benjamin.kim.nguyen@gmail.com

[[Algolia] Technical test - Analytics · GitHub](https://gist.github.com/sfriquet/55b18848d6d58b8185bbada81c620c4a)

This project extract the popular HN queries done during a specific time range from a given log dataset.

## Setup

This project has been written in javascript and more particularly on node 10.16.3

# Get started

inside the project run

```
yarn or npm install
```

if all tests are green using

```
yarn test or npm test
```

you can start the server using

```
yarn start or npm start
```

Then you can use these queries to call the server:

```
GET /1/queries/count/2015
GET /1/queries/count/2015-08
GET /1/queries/count/2015-08-03
GET /1/queries/count/2015-08-01 00:04
GET /1/queries/popular/2015?size=3
GET /1/queries/popular/2015-08-02?size=5
```

## The algorithm

### choice made

I did not use createReadStream because it send chunk of date and the strucure we need which is line is not preserved  
I use a javascript object to hold the data before the sort with the HNquery as a key and the number of time the query was made as a value bacause I will access the value a lot of time and the access is cheap.

### misc

The verif.js script is used to verify if the file dates are in a good format
I didn't take the versionning of the api into account in the test
I didn't have a proper error handling in the test
