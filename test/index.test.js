const { compute } = require("../lib");

const expectedResultFor2015 = [
  {
    query: "http%3A%2F%2Fwww.getsidekick.com%2Fblog%2Fbody-language-advice",
    count: 6675
  },
  { query: "http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F568045", count: 4652 },
  {
    query: "http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F379035%3Fsort%3D1",
    count: 3100
  }
];

const expectedResultFor20150802 = [
  {
    query: "http%3A%2F%2Fwww.getsidekick.com%2Fblog%2Fbody-language-advice",
    count: 2283
  },
  { query: "http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F568045", count: 1943 },
  {
    query: "http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F379035%3Fsort%3D1",
    count: 1358
  },
  {
    query:
      "http%3A%2F%2Fjamonkey.com%2F50-organizing-ideas-for-every-room-in-your-house%2F",
    count: 890
  },
  {
    query:
      "http%3A%2F%2Fsharingis.cool%2F1000-musicians-played-foo-fighters-learn-to-fly-and-it-was-epic",
    count: 701
  }
];

test("computing with query=2015 and size=3", done => {
  compute("2015", { size: 3 }).then(res => {
    expect(res).toStrictEqual(expectedResultFor2015);
    done();
  });
});
test("computing with query=2015 and options={isCountOnly: true}", done => {
  compute("2015", { isCountOnly: true }).then(res => {
    expect(res).toStrictEqual({ count: 573697 });
    done();
  });
});
test("computing with query=2015-08 and options={isCountOnly: true}", done => {
  compute("2015-08", { isCountOnly: true }).then(res => {
    expect(res).toStrictEqual({ count: 573697 });
    done();
  });
});
test("computing with query=2015-08-03 and options={isCountOnly: true}", done => {
  compute("2015-08-03", { isCountOnly: true }).then(res => {
    expect(res).toStrictEqual({ count: 198117 });
    done();
  });
});
test("computing with query=2015-08-01 00:04 and options={isCountOnly: true}", done => {
  compute("2015-08-01 00:04", { isCountOnly: true }).then(res => {
    expect(res).toStrictEqual({ count: 617 });
    done();
  });
});
test("computing with query=2015-08-02 and size=5", done => {
  compute("2015-08-02", { size: 5 }).then(res => {
    expect(res).toStrictEqual(expectedResultFor20150802);
    done();
  });
});
