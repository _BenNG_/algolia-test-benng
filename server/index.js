const express = require("express");
const app = express();
const port = 3000;
const { queriesRoutes } = require("./v1/queries/routes");

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

app.use("/1/queries", queriesRoutes);
