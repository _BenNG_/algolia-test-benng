const express = require("express");

const { count, popular } = require("../controller");

const queriesRoutes = express.Router();

queriesRoutes.get("/count/:query", count);
queriesRoutes.get("/popular/:query", popular);

module.exports = {
  queriesRoutes
};
