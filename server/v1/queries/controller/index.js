const { compute } = require("../../../../lib/");

const count = async (req, res) => {
  const query = req.params.query;
  if (!query) {
    res.status(404).end();
  }
  try {
    const resp = await compute(query, { isCountOnly: true });
    res.json(resp);
  } catch (e) {
    throw new Error(e);
  }
};

const popular = async (req, res) => {
  const query = req.params.query;
  const size = parseInt(req.query.size, 10);
  if (!query) {
    res.status(404).end();
  }
  if (isNaN(size)) {
    throw new Error("size must be a number");
  }

  try {
    const resp = await compute(query, { size: parseInt(size, 10) });
    res.json(resp);
  } catch (e) {
    throw new Error(e);
  }
};

module.exports = {
  count,
  popular
};
